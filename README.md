An atom text editor package that allows users to connect a socket.io server and send code snippets to each other to ask for help.

Before you start using it, make sure you have set the configuration values below.
<ul>
<li><code>Username</code></li>
<li><code>Server Address</code></li>
<li><code>Connection Key</code></li>
</ul>  
  
<blockquote>This repository contains only client-side atom package files. The server-side project will be added soon.</blockquote>
