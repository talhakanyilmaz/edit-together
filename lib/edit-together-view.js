'use babel';

export default class EditTogetherView {

  username = null;

  constructor(serializedState) {
    // Create root element
    this.element = document.createElement('div');
    this.element.classList.add('edit-together');
    this.element.style.width = "100%";
    this.element.style.height = "250px";
    // Create message element
    this.username = atom.config.get('edit-together.username');
    let username = document.createElement('h1');
    username.textContent = this.username;
    this.element.appendChild(username);
  }

  serialize() {}

  destroy() {
    this.element.remove();
  }

  getElement() {
    return this.element;
  }


}
