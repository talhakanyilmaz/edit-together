'use babel';

import EditTogetherView from './edit-together-view';
import { CompositeDisposable } from 'atom';
import io from 'socket.io-client';

export default {

  socket: null,
  status: null,
  editTogetherView: null,
  subscriptions: null,
  socket_id: null,
  server_address: null,
  username: null,
  chatPanel: null,
  grammar: null,
  first_id: null,

  activate(state) {
    this.socket = null;
    let editor = atom.workspace.getActiveTextEditor();
    this.grammar = editor.getGrammar().name;
    this.username = atom.config.get('edit-together.username');
    this.server_address = atom.config.get('edit-together.server_address');


    this.editTogetherView = new EditTogetherView(state.editTogetherViewState);
    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new CompositeDisposable();
    this.chatPanel = atom.workspace.addFooterPanel({
      item: this.editTogetherView.getElement(),
      visible: true
    });

    // Register command that toggles this view
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'edit-together:start_app': () => this.start_app(),
      'edit-together:connect': () => this.connect(),
      'edit-together:share': () => this.share(),
      'edit-together:disconnect': () => this.disconnect(),
      'edit-together:openChatPanel': () => this.openChatPanel()
    }));
  },

  deactivate() {
    this.subscriptions.dispose();
    this.editTogetherView.destroy();
  },

  serialize() {
    return {
      editTogetherViewState: this.editTogetherView.serialize()
    };
  },

  start_app() {
    if (this.username === "user" || this.server_address === "not-set") {
      atom.notifications.addError('You need to set Username and Server Address!');
    }
    else {
      atom.notifications.addSuccess('Ready to Connect!');
      this.status = "started";
    }
  },

  connect() {
    if (this.status != "started") {
      atom.notifications.addError('You need to Start edit-together App!');
    }
    else {
      try{
        this.socket = io.connect(this.server_address);
        this.socket.emit('edit-together', { grammar: this.grammar});
        this.socket.on('edit-together', (response) => {
          if (response === "connected") {
            atom.notifications.addSuccess(this.server_address+'\nConnection Established.');
            this.socket_id = this.socket.id;
            this.first_id = atom.config.get('edit-together.firstID');
            this.socket.emit('new_user', {new_user: this.username, first_id: this.first_id});
            this.socket.on('login', (res) => {
              if (res === "accepted") {
                atom.notifications.addSuccess(this.server_address+'\nYou are logged in!.');
              }
              else {
                atom.config.set('edit-together.firstID', this.socket_id);
                atom.notifications.addSuccess(this.server_address+'\nWELCOME.');
              }
            });
            this.status = "connected";
          }
        });
        this.socket.on('disconnect', () => {
          atom.notifications.addWarning('You have disconnected from the edit-together Server');
        });
      }
      catch(Exception) {
        atom.notifications.addError("An error has occured!");
      }
    }
  },

  openChatPanel() {
    this.chatPanel.isVisible() ?
    this.chatPanel.hide() :
    this.chatPanel.show()
  },

  disconnect() {
    this.socket.emit('disconnection_request');
  }

};
